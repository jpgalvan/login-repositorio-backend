package com.login.backend.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.login.backend.model.Usuario;
import com.login.backend.service.UsuarioService;

@RestController
@RequestMapping("usuario")
public class UsuarioRest {

	private String key = "pvNF#$y*J=_N9h^x6t2J%ebv*HP8A";
	private PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
	private SimpleStringPBEConfig config = new SimpleStringPBEConfig();

	@Autowired
	UsuarioService usuarioService;

	public UsuarioRest() {
		config.setPassword(key);
		config.setAlgorithm("PBEWithMD5AndDES");
		config.setKeyObtentionIterations("1000");
		config.setPoolSize("1");
		config.setProviderName("SunJCE");
		config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
		config.setStringOutputType("base64");
		encryptor.setConfig(config);
	}

	@GetMapping
	public ResponseEntity<List<Usuario>> getAll() {
		return ResponseEntity.ok(usuarioService.findAll());
	}

	@PostMapping
	public ResponseEntity<Usuario> newUser(@RequestBody Usuario u) {
		try {
			u.setPassword(encryptor.encrypt(u.getPassword()));
			Usuario saved = usuarioService.save(u);
			return ResponseEntity.created(new URI("/usuario/" + saved.getId())).body(saved);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PostMapping("/auth/signin")
	public ResponseEntity<Optional<Usuario>> authUser(@RequestBody Usuario u) {
		try {

			Optional<Usuario> found = usuarioService.findByUsername(u.getUsuario());
			if (found.isPresent()) {
				if (u.getPassword().equals(encryptor.decrypt(found.get().getPassword())))
					return ResponseEntity.ok(found);
				else
					return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
			} else
				return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

}
