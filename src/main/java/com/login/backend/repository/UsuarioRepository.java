package com.login.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.login.backend.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	@Query(value = "SELECT * FROM usuarios WHERE usuario = ?1 and password = ?2", nativeQuery = true)
	Optional<Usuario> authUser(String usuario, String password);
	
	@Query(value = "SELECT * FROM usuarios WHERE UPPER(usuario) = UPPER(?1)", nativeQuery = true)
	Optional<Usuario> findByUsername(String usuario);
}
